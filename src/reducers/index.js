const reducer = (state, action) => {
    switch (action.type) {
        case 'DELETE_PAGINA':
            return {
                ...state,
                pagina: state.pagina.filter(
                    (index) => false,
                ),
            };
        case 'SET_PRELOADER':
            return {
                ...state,
                preloader: action.payload
            };
        case 'SET_CURRENT_PAGE':
            return {
                ...state,
                currentPage: action.payload
            };
            case 'SET_SHOW':
                return {
                    ...state,
                    show: action.payload
                };
                

        case 'SET_PAGE':
            return {
                ...state,
                page: action.payload
            };
        case 'SET_CONTENT':
            return {
                ...state,
                content: action.payload
            }
        case 'SET_LIST_INSURANCES':
            return {
                ...state,
                listInsurances: action.payload
            }
        case 'SET_COBERTURAS':
            return {
                ...state,
                coberturas: action.payload
            }

        case 'SET_KEYTAB':
                return {
                    ...state,
                    keytab: action.payload
                }
        default:
            return state;
    }
};

export default reducer;