export const setPreloader = (payload) => ({
  type: "SET_PRELOADER",
  payload,
});

export const setPage = (payload) => ({
  type: "SET_PAGE",
  payload,
});

export const setContent = (payload) => ({
  type: "SET_CONTENT",
  payload,
});

export const setListInsurances = (payload) => ({
  type: "SET_LIST_INSURANCES",
  payload,
});
export const setShow = (payload) => ({
  type: "SET_SHOW",
  payload,
});
export const setCoberturas = (payload) => ({
  type: "SET_COBERTURAS",
  payload,
});
export const setKeytab = (payload) => ({
  type: "SET_KEYTAB",
  payload,
});
