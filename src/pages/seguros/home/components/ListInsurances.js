import { Card, Col, Row, Button } from "react-bootstrap";
import CardInfo from "../../components/CardInfo";
import "../../../../assets/sass/components/generales/box_information.scss"
import cleanInsuranceName from "../../../../utils/cleanInsuranceName";
import mala from "../../../../assets/img/icon_mala.png";
import {API_URL} from "../../../../config";

const ListInusrances = ({ list, icon, keytab }) => {


    if(API_URL == "https://itauapiqa.alwayson.cl/api"){

    //Links directos
        var autoURL = "https://itausegurostest.alwayson.cl/sitio_privado_front/test/build/index.html";
        var proteccionURL= "https://itausegurostest.alwayson.cl/sitio_privado_front/proteccion/build/index.html";
        var hogarURL= "https://itausegurostest.alwayson.cl/sitio_privado_front/hogar/build/index.html";
    //Link solo a vitrina
        var vitrinaURL = "https://itausegurostest.alwayson.cl/sitio_privado_front/build/index.html"
    //Link a vitrina + parametro
        var paramURL= "https://itausegurostest.alwayson.cl/sitio_privado_front/build/index.html" + "?keytab=" + keytab;
    } else{

    //Links directos
        var autoURL = "https://itauseguros.alwayson.cl/itau/13_VENTAS/seguros/productos/SVBBSS035/index.html";
        var proteccionURL= "https://itauseguros.alwayson.cl/sitio_privado/proteccion/index.html";
        var hogarURL= "https://itauseguros.alwayson.cl/itau/13_VENTAS/seguros/productos/SVBBSS023_r/index.html";
    //Link solo a vitrina
        var vitrinaURL = "https://itauseguros.alwayson.cl/sitio_privado/index.html"
    //Link a vitrina + parametro
        var paramURL= "https://itauseguros.alwayson.cl/sitio_privado/index.html" + "?keytab=" + keytab;

    }
    //Especie de Switch
    const urlTab ={
        'auto' :autoURL,
        'hogar' : hogarURL,
        'proteccion' : proteccionURL,
        'vida' : paramURL,
        'salud' : paramURL,
        'otros' : vitrinaURL,
        'asociados_credito' : vitrinaURL
    }
    const url = urlTab[keytab]

    return (
        <div key={`keytab-${keytab}`}>
            {
                list.length > 0 ?
                    list.map((item, i) => {
                        var prima_uf = item.prima_bruta ? item.prima_bruta.toString().replace(/\./g, ',') : null;
                        return (
                            <div key={`div-frag-${i}`}>
                                {
                                    <div key={`div-${i}`}>
                                        <Row key={`row-${i}`}>
                                            <Col key={`col-${i}`}>
                                                <CardInfo key={`CardInfo-${i}`}
                                                    Title={typeof item.segNombre == 'undefined' ? item.tipo_seguro : cleanInsuranceName(item.segNombre)}
                                                    Parag={typeof item.num_poliza != 'undefined' ? item.num_poliza ? item.num_poliza : null : null}
                                                    Parag2={item.fecha_vigencia ? item.fecha_vigencia ? item.fecha_vigencia : null : null}
                                                    Parag3={typeof item.materia_asegurada == 'undefined' ? null : item.materia_asegurada ? item.materia_asegurada : null}
                                                    Parag4={prima_uf ? prima_uf : null}
                                                    Parag5={typeof item.patente == 'undefined' ? null : item.patente ? item.patente : null}
                                                    Parag6={typeof item.cantidad != 'undefined' ? item.cantidad : null}
                                                    icon={icon}
                                                    item={item}
                                                    codProduct={typeof item.cod_producto == 'undefined' ? "SVBBSS035" : item.cod_producto}
                                                    rut_tercero={typeof item.tercero != 'undefined' ? item.tercero : null}
                                                    estado_seguro={typeof item.estadoSeguro != 'undefined' ? item.estadoSeguro : null}
                                                    fecha_contratacion={typeof item.fecha_contratacion != 'undefined' ? item.fecha_contratacion : null}
                                                    keytab={typeof keytab != 'undefined' ? keytab : null}
                                                    tipo_seguro={typeof item.tipo_seguro != 'undefined' ? item.tipo_seguro : null}
                                                />
                                            </Col>
                                        </Row>
                                    </div>
                                }
                            </div>
                        );
                    })
                    :
                    <div key={`noList-${keytab}`} className="box_information">
                        <Row key={`noList-${keytab}`}>
                            <Col key={`noList-${keytab}`} xs={12} className="text-center">
                                <img src={mala}></img>
                                <div className={'title_information '} ><h6 className={'title_information '} >No posees seguros contratados</h6></div>
                                <div className="content_information" >
                                    <p>Asegura tu tranquilidad y la de tu familia.</p>
                                </div>
                                <div className="box_information_button">
                                    <Button className="btn" variant="outline-info" href={url}>Ir a Seguros</Button> 
                                </div>
                            </Col>
                        </Row>
                    </div>
            }
        </div>
    );
}

export default ListInusrances;