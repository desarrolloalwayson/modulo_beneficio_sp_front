import React, { useEffect, useState } from "react";
import { Container, Tab, Tabs } from "react-bootstrap";
import ListInsurances from "./ListInsurances";
import '@sass-components/generales/box_query_insurance.scss'
import { useSelector } from "react-redux";

const TabInsurances = () => {

    const insurancesList = useSelector(state => state.listInsurances)
    const keytab = useSelector(state => state.keytab)
    const [firstKeySet, setfirstKeySet] = useState(keytab ? keytab : "auto")
    useEffect(() => {
        if (Object.keys(insurancesList).length > 0 && !keytab) {
            if (insurancesList["auto"].list_insurances.length > 0) {
                setfirstKeySet("auto")
            }else if(insurancesList["vida"].list_insurances.length > 0){
                setfirstKeySet("vida")
            }else if(insurancesList["hogar"].list_insurances.length > 0){
                setfirstKeySet("hogar")
            }else if(insurancesList["salud"].list_insurances.length > 0){
                setfirstKeySet("salud")
            }else if(insurancesList["proteccion"].list_insurances.length > 0){
                setfirstKeySet("proteccion")
            }else if(insurancesList["asociados_credito"].list_insurances.length > 0){
                setfirstKeySet("asociados_credito")
            }else if(insurancesList["otros"].list_insurances.length > 0){
                setfirstKeySet("otros")
            }else{
                setfirstKeySet("auto")
            }
        }
    }, [])

    return (
        <>
            <Container id="main" fluid="fluid">
                {
                    typeof insurancesList == 'undefined' ?
                        <div className="box-insurance-query">
                            <div className="box-insurance-query">
                                <Tabs defaultActiveKey="auto" id="seguros-tab-example" className="mb-3">
                                    <Tab eventKey="auto" title="Automotriz" tabClassName="orange noRecords">
                                        <ListInsurances list={[]} icon="icon-itaufonts_full_seguro_auto" keytab="auto" />
                                    </Tab>
                                    <Tab eventKey="vida" title="Vida" tabClassName="green noRecords">
                                        <ListInsurances list={[]} icon="icon-itaufonts_seguro_mulher" keytab="vida" />
                                    </Tab>
                                    <Tab eventKey="hogar" title="Hogar" tabClassName="blue noRecords">
                                        <ListInsurances list={[]} icon="icon-itaufonts_seguro_residencia" keytab="hogar" />
                                    </Tab>
                                    <Tab eventKey="salud" title="Salud" tabClassName="blue noRecords">
                                        <ListInsurances list={[]} icon="icon-itaufonts_seguro_vida" keytab="salud" />
                                    </Tab>
                                    <Tab eventKey="proteccion" title="Protección" tabClassName="black noRecords">
                                        <ListInsurances list={[]} icon="icon-itaufonts_seguro_cartao" keytab="proteccion" />
                                    </Tab>      
                                    <Tab eventKey="asociados_credito" title="Asociado a crédito" tabClassName="yellow noRecords">
                                        <ListInsurances list={[]} icon="icon-itaufonts_seguro_vida" keytab="asociados_credito" />
                                    </Tab>
                                    <Tab eventKey="otros" title="Otros" tabClassName="brown noRecords">
                                        <ListInsurances list={[]} icon="icon-itaufonts_seguro_vida" keytab="otros" />
                                    </Tab>
                                </Tabs>
                            </div>
                        </div>
                        :
                        Object.keys(insurancesList).length > 0 ?
                            <div className="box-insurance-query">
                                <Tabs activeKey={firstKeySet} id="seguros-tab-example" className="mb-3" onSelect={(k) => setfirstKeySet(k)}>
                                    <Tab eventKey="auto" title="Automotriz" tabClassName={insurancesList.auto.list_insurances.length > 0 ? 'orange' : 'orange noRecords'}>
                                        <ListInsurances list={insurancesList.auto.list_insurances} icon="icon-itaufonts_full_seguro_auto" keytab="auto" />
                                    </Tab>
                                    <Tab eventKey="vida" title="Vida" tabClassName={insurancesList.vida.list_insurances.length > 0 ? 'green' : 'green noRecords'}>
                                        <ListInsurances list={insurancesList.vida.list_insurances} icon="icon-itaufonts_seguro_mulher" keytab="vida" />
                                    </Tab>
                                    <Tab eventKey="hogar" title="Hogar" tabClassName={insurancesList.hogar.list_insurances.length > 0 ? 'blue' : 'blue noRecords'}>
                                        <ListInsurances list={insurancesList.hogar.list_insurances} icon="icon-itaufonts_seguro_residencia" keytab="hogar" />
                                    </Tab>
                                    <Tab eventKey="salud" title="Salud" tabClassName={insurancesList.salud.list_insurances.length > 0 ? 'brown' : 'brown noRecords'}>
                                        <ListInsurances list={insurancesList.salud.list_insurances} icon="icon-itaufonts_seguro_vida" keytab="salud" />
                                    </Tab>
                                    <Tab eventKey="proteccion" title="Protección" tabClassName={insurancesList.proteccion.list_insurances.length > 0 ? 'black' : 'black noRecords'}>
                                        <ListInsurances list={insurancesList.proteccion.list_insurances} icon="icon-itaufonts_seguro_cartao" keytab="proteccion" />
                                    </Tab>                                  
                                    <Tab eventKey="asociados_credito" title="Asociado a crédito" tabClassName={insurancesList.asociados_credito.list_insurances.length > 0 ? 'yellow' : 'yellow noRecords'}>
                                        <ListInsurances list={insurancesList.asociados_credito.list_insurances} icon="icon-itaufonts_seguro_vida" keytab="asociados_credito" />
                                    </Tab>
                                    <Tab eventKey="otros" title="Otros" tabClassName={insurancesList.otros.list_insurances.length > 0 ? 'brown' : 'brown noRecords'}>
                                        <ListInsurances list={insurancesList.otros.list_insurances} icon="icon-itaufonts_seguro_vida" keytab="otros" />
                                    </Tab>
                                </Tabs>
                            </div>
                            :
                            <div className="box-insurance-query">
                                <div className="box-insurance-query">
                                    <Tabs defaultActiveKey="auto" id="seguros-tab-example" className="mb-3">
                                        <Tab eventKey="auto" title="Automotriz" tabClassName="orange noRecords">
                                            <ListInsurances list={[]} icon="icon-itaufonts_full_seguro_auto" keytab="auto" />
                                        </Tab>
                                        <Tab eventKey="vida" title="Vida" tabClassName="green noRecords">
                                            <ListInsurances list={[]} icon="icon-itaufonts_seguro_mulher" keytab="vida" />
                                        </Tab>
                                        <Tab eventKey="hogar" title="Hogar" tabClassName="blue noRecords">
                                            <ListInsurances list={[]} icon="icon-itaufonts_seguro_residencia" keytab="hogar" />
                                        </Tab>
                                        <Tab eventKey="salud" title="Salud" tabClassName="blue noRecords">
                                            <ListInsurances list={[]} icon="icon-itaufonts_seguro_vida" keytab="salud" />
                                        </Tab>
                                        <Tab eventKey="proteccion" title="Protección" tabClassName="black noRecords">
                                            <ListInsurances list={[]} icon="icon-itaufonts_seguro_cartao" keytab="proteccion" />
                                        </Tab>                                  
                                        <Tab eventKey="asociados_credito" title="Asociado a crédito" tabClassName="yellow noRecords">
                                            <ListInsurances list={[]} icon="icon-itaufonts_seguro_vida" keytab="asociados_credito" />
                                        </Tab>
                                        <Tab eventKey="otros" title="Otros" tabClassName="brown noRecords">
                                            <ListInsurances list={[]} icon="icon-itaufonts_seguro_vida" keytab="otros" />
                                        </Tab>
                                    </Tabs>
                                </div>
                            </div>
                }
            </Container>
        </>
    );
}

export default TabInsurances;