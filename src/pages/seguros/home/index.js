import React from "react";
import { Container, Card } from 'react-bootstrap';
/* Import componente */
import TabInsurances from "./components/TabInsurances";
/* Estilos */
import "@sass-components/generales/cardInfo.scss";
import "@sass-components/generales/reminder_box.scss";

const Index = () => {
  return (
    <>
      <h3 className="title_sure">MIS SEGUROS</h3>
      <TabInsurances />

      <Container id="main" fluid="fluid">
        <Card className="reminder_box">
          <i className={'icon icon-itaufonts_informacao'}></i>
          <p>Recuerda que al contratar tus productos por medios que no sean presenciales contarás con 35 días desde que tomaste conocimiento de la póliza respectiva o desde la recepción de tu póliza vía correo electrónico para ejercer tu derecho de retractación en dicha contratación.</p>
        </Card>
      </Container>
    </>
  )
}

export default Index
