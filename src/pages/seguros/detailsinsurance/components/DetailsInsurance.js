import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import CardInfo from "../../components/CardInfo";
import { Container, Button } from "react-bootstrap";
import { setPage } from "../../../../actions";
import { Modal } from "react-bootstrap";
import { cod_eliminarSeguro } from "../../../../utils/codEliminarSeguroAlternativo";
import { Modal_eliminarSeguro } from "../../components/modals/modal_eliminarSeguro";

/* Import IMG */
import zurich from "../../../../assets/img/segurosAuto/zurich.png";
import consorcio from "../../../../assets/img/segurosAuto/consorcio.png";
import hdi from "../../../../assets/img/segurosAuto/hdi.png";
import liberty from "../../../../assets/img/segurosAuto/liberty.png";
import mapfre from "../../../../assets/img/segurosAuto/mapfre.png";
import sura from "../../../../assets/img/segurosAuto/sura.png";
import zenit from "../../../../assets/img/segurosAuto/zenit.png";
import cardif from "../../../../assets/img/segurosAuto/cardif.jpg";
import metlife from "../../../../assets/img/segurosAuto/metlife.jpg";
import fourlife from "../../../../assets/img/segurosAuto/4life.jpg";
import savebcj from "../../../../assets/img/segurosAuto/savebcj.png";

function DetailsInsurance() {
  const content = useSelector((state) => state.content)
  const [disableButton, setdisableButton] = useState(false)
  const dispatch = useDispatch();
  const backHome = () => {
    dispatch(setPage("PRELOADER"));
    setTimeout(() => {
      dispatch(setPage("HOME"));
    }, 2000);
  };
  const buttonLoaderAndDisable = (url, type) => {
    dispatch(setPage("PRELOADER"));
    setdisableButton(true)
    window.location.replace(url)
  }

  const urlEliminacion = content.url_eliminacion == 'undefined' ? null : content.url_eliminacion
  //Para el modal de eliminar seguro
  const codProduct = content.cod_producto

  const listImgCompany = {
    "consolidada" : zurich,
    "zurich" : zurich,
    "zenit" : zenit,
    "mapfre" : mapfre,
    "hdi" : hdi,
    "sura" : sura,
    "liberty" : liberty,
    "consorcio" : consorcio,
    "cardif" : cardif,
    "metlife" : metlife,
    "4life" : fourlife,
    "itau" : savebcj,
  }
  const companyImg = listImgCompany[content.compania_corto.toLowerCase()] ? listImgCompany[content.compania_corto.toLowerCase()] : content.company.toLowerCase();
  return (
    <>
      <Container id="main" fluid="fluid">

        <CardInfo
          Title={content.company}
          dateC={content.fecha_contratacion}
          dateV={content.fecha_vigencia ? content.fecha_vigencia : null}
          dateR={content.fecha_renovacion}
          dateB={content.bonificacion}
          nAccount={typeof content.num_pago == 'undefined' ? null : content.num_pago ? content.num_pago.substr(content.num_pago.length - 4) : null}
          codProduct={typeof content.cod_producto == 'undefined' ? "SVBBSS035" : content.cod_producto}
          mAccount={typeof content.num_pago == 'undefined' ? null : content.tipo_cuenta ? content.tipo_cuenta : null}
          tAccount={typeof content.metodo_pago == 'undefined' ? null : content.metodo_pago ? content.metodo_pago : null}
          url_siniestros={typeof content.url_siniestros == 'undefined' ? null : content.url_siniestros}
          url_eliminar={typeof content.url_eliminacion == 'undefined' ? null : content.url_eliminacion}
          estado_seguro={content.estadoSeguro}
          img={typeof content.compania_corto == 'undefined' ? null : companyImg}
        />
        <div className="btn-derecha">
          
          {//Condicional del boton eliminar seguro
          
          cod_eliminarSeguro.includes(codProduct.toUpperCase()) ?
          <Modal_eliminarSeguro /> :
          <Button disabled={disableButton} onClick={() => buttonLoaderAndDisable(urlEliminacion+"&type_requirement=eliminacion_de_seguro")} className="btn-eliminar text-center" variant="inline-black">Eliminar seguro</Button>}
        
          
          <Button className="btn" variant="outline-info" onClick={backHome}>Volver</Button>
        </div>
      </Container>
    </>
  );
}

export default DetailsInsurance;