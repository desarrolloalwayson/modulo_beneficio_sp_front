import React from "react";
import { useSelector } from "react-redux";
import cleanInsuranceName from "../../../utils/cleanInsuranceName";
import "@sass-components/generales/cardInfo.scss";
/* Estilos */
import Voucher from "./components/Voucher";

const Index = () => {
  const {segNombre} = useSelector((state) => state.content)
  return (
    <>
      <h3 className="title_sure">{cleanInsuranceName(segNombre)}</h3>
      <Voucher />
    </>
  )
}

export default Index
