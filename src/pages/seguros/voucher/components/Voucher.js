import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import CardInfo from "../../components/CardInfo";
import { Container, Button } from "react-bootstrap";
import { setPage } from "../../../../actions";

/* Import IMG */
import consorcio from "../../../../assets/img/segurosAuto/consorcio.png";
import zurich from "../../../../assets/img/segurosAuto/zurich.png";
import hdi from "../../../../assets/img/segurosAuto/hdi.png";
import liberty from "../../../../assets/img/segurosAuto/liberty.png";
import mapfre from "../../../../assets/img/segurosAuto/mapfre.png";
import sura from "../../../../assets/img/segurosAuto/sura.png";
import zenit from "../../../../assets/img/segurosAuto/zenit.png";
import cardif from "../../../../assets/img/segurosAuto/cardif.jpg";
import metlife from "../../../../assets/img/segurosAuto/metlife.jpg";
import fourlife from "../../../../assets/img/segurosAuto/4life.jpg";
import savebcj from "../../../../assets/img/segurosAuto/savebcj.png";

function Voucher() {
  const content = useSelector((state) => state.content)
  const dispatch = useDispatch();
  const backHome = () => {
    dispatch(setPage("PRELOADER"));
    setTimeout(() => {
      dispatch(setPage("HOME"));
    }, 2000);
  };
  const print = () => { window.print() }
 const listImgCompany = {
    "consolidada" : zurich,
    "zenit" : zenit,
    "zurich" : zurich,
    "mapfre" : mapfre,
    "hdi" : hdi,
    "sura" : sura,
    "liberty" : liberty,
    "consorcio" : consorcio,
    "cardif" : cardif,
    "metlife" : metlife,
    "4life" : fourlife,
    "itau" : savebcj,
  }
  const companyImg = listImgCompany[content.compania_corto.toLowerCase()] ? listImgCompany[content.compania_corto.toLowerCase()] : content.company.toLowerCase();
  return (
    <>
      <Container id="main" fluid="fluid">

        <CardInfo
          Title={content.company}
          dateC={content.fecha_contratacion}
          dateV={content.fecha_vigencia ? content.fecha_vigencia : null}
          dateR={content.fecha_renovacion}
          dateB={content.bonificacion}
          Parag={typeof content.num_poliza != 'undefined' ? content.num_poliza ? content.num_poliza : null : null}
          Parag2={content.fecha_vigencia ? content.fecha_vigencia : null}
          nAccount={typeof content.num_pago == 'undefined' ? null : content.num_pago ? content.num_pago.substr(content.num_pago.length - 4) : null}
          codProduct={typeof content.cod_producto == 'undefined' ? "SVBBSS035" : content.cod_producto}
          mAccount={typeof content.num_pago == 'undefined' ? null : content.tipo_cuenta ? content.tipo_cuenta : null}
          tAccount={typeof content.metodo_pago == 'undefined' ? null : content.metodo_pago ? content.metodo_pago : null}
          url_siniestros={typeof content.url_siniestros == 'undefined' ? null : content.url_siniestros}
          url_eliminar={typeof content.url_eliminacion == 'undefined' ? null : content.url_eliminacion}
          prima_neta_mensual={content.prima_neta == 'undefined' ? null : content.prima_neta}
          prima_bruta_mensual={content.prima_bruta == 'undefined' ? null : content.prima_bruta}
          rut={content.rut}
          fecha_fin_poliza={content.fecha_fin}
          nombre_completo={content.nombre_completo == 'undefined' ? null : content.nombre_completo}
          comision={content.comision == 'undefined' ? null : content.comision}
          img={typeof content.compania_corto == 'undefined' ? null : companyImg}
        />
        <div className="btn-derecha" id="printPageButton">
          <Button className="btn"  variant="outline-primary" onClick={print} >Imprimir</Button>
          <Button className="btn"  variant="outline-info" onClick={backHome}>Volver</Button>
        </div>
      </Container>
    </>
  );
}

export default Voucher