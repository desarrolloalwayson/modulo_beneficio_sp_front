/* Aqui deberian ir todas las paginas del proyecto eredar la informacion desde aqui
 */
import React, { useEffect } from "react";
import HomeSeguros from "./home/index";
import DetailsInsurance from "./detailsinsurance/index";
import Voucher from "./voucher/index";
import BoxError from "./components/box_error";
import BoxErrorNoInsurance from "./components/box_error_noinsurance";
import { useSelector, useDispatch } from "react-redux";
import { setListInsurances, setPage, setPreloader } from "../../actions";
import Preloader from '../../components/preloader';
import Insurances from "../../services/Insurances";
import ModalInfo from "../../pages/seguros/components/modals/modal_info";
import BoxPolicyDownload from "./components/box_policy_download";
import BoxClientMessage from "./components/box_client_message";


const Seguros = () => {
  const { getInsurances } = Insurances();
  const dispatch = useDispatch();
  const page = useSelector((state) => state.page)

  let pageUrl = 'HOME';

  useEffect(() => {
    getInsurances();

  }, [])

  return (
    <div>
      <ModalInfo/>
      {page === "ERROR_NOINSURANCE" ? <BoxErrorNoInsurance /> : <div></div>}
      {page === "POLICY_DOWNLOAD" ? <BoxPolicyDownload /> : <div></div>}
      {page === "CLIENT_MESSAGE" ? <BoxClientMessage /> : <div></div>}
      {page === "ERROR" ? <BoxError /> : <div></div>}
      {page === "PRELOADER" ? <Preloader /> : <div></div>}
      {page === "HOME" ? <HomeSeguros /> : <div></div>}
      {page === "DETAILS_INSURANCE" ? <DetailsInsurance /> : <div></div>}
      {page === "VOUCHER" ? <Voucher/>: <div></div> }
    </div>
  )
}

export default Seguros
