import React, { useState } from 'react'; import Modal from 'react-bootstrap/Modal';

import {  Button } from "react-bootstrap";
 
 export function Modal_eliminarSeguro() {
    const [show, setShow] = useState(false);
  
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
  
    return (
      <>
        <Button className="btn-eliminar text-center" variant="inline-black" onClick={handleShow}>
          Eliminar seguro
        </Button>
  
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Estimado cliente: </Modal.Title>
          </Modal.Header>
          <Modal.Body>Para eliminar este tipo de seguro, debes acercarte a tu ejecutivo de cuenta y solicitar la eliminación.</Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={handleClose}>
              Aceptar
            </Button>
            
          </Modal.Footer>
        </Modal>
      </>
    );
  }