import React, { useState } from 'react'; import Modal from 'react-bootstrap/Modal';
import { useDispatch, useSelector } from "react-redux";
import { setShow } from "../../../../actions";
import { Col, Button, Table } from "react-bootstrap";
/* Estilos */
import '@sass-components/modalTr.scss'
import moment from 'moment';

const ModalInfo = () => {
  const show = useSelector((state) => state.show);
  const content = useSelector((state) => state.coberturas);
  const dispatch = useDispatch();
  const handleClose = () => {
    dispatch(setShow(false))
  };

  return (
    <div >{show === false ? (<div></div>) :
      (<div>
        <Modal size="lg"
          show={show}
          onHide={handleClose}
          animation={false}
        >
          <Modal.Body>
            {
              <div className='style1'>
                <Table>
                  <thead>
                    <tr>
                      <th>
                        {
                          typeof content == 'undefined' ?
                            ''
                            :
                            <div className='text-capitalize'>
                              {content.tipo}
                            </div>
                        }
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        {
                          typeof content == 'undefined' ?
                            ''
                            :
                            (<ul className='list-orange'>
                              {typeof content.detalles == 'string'   ?
                                <li>{content.detalles}</li>
                                :
                                (content.detalles.length > 0 && content.detalles != "N" ?
                                  content.detalles.map((item) => {
                                    return (
                                      <li>{item.toUpperCase()}</li>
                                    )
                                  }) :
                                  ""
                                )
                              }
                              {
                                (content.comp && content.comp != "N" && content.tipo== 'asistencia' )?

                                  <li>Asistencia (Compañía {content.comp})</li>
                                  :
                                  ""
                              }
                              {
                                (content.phone && content.phone != "N" && content.tipo== 'asistencia' ) ?

                                  <li>Fono: {content.phone}</li>
                                  :
                                  ""
                              }
                              {content.detalles =="N" && content.comp =="N" && content.phone =="N" ?
                              `El seguro no posee ${content.tipo == 'coberturas' ? 'coberturas' : 'asistencias'} asociadas` :
                              ""
                              }
                            </ul>)
                        }
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </div>
            }
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={handleClose} className="modal_info_footer">
              Cerrar
            </Button>
          </Modal.Footer>
        </Modal> </div>)
    }
    </div>
  );
};
export default ModalInfo;