import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { setPage, setKeytab } from "../../../actions";
/* Estilos */
import { Row, Col, Button, Card, Container } from "react-bootstrap";
import "../../../assets/sass/components/generales/box_policy_download.scss";
import "../../../assets/sass/components/generales/cardInfo.scss";
/*Ticket*/
import ticket from "../../../assets/img/ticket.png";

const BoxPolicyDownload = (props) => {
    const dispatch = useDispatch();
    const backHome = () => {
        dispatch(setPage("PRELOADER"));
        setTimeout(() => {
            dispatch(setPage("HOME"));
        }, 2000);
    };
    const content = useSelector((state) => state.content)

    if (content.tipo_seguro === 'AsociadosCreditos'){
        dispatch(setKeytab('asociados_credito'));
    }else if (content.tipo_seguro === 'Creditos'){
        dispatch(setKeytab('otros'));
    } else {
        dispatch(setKeytab(content.tipo_seguro.toLowerCase()));
    }
    return (
        <>
        <br></br>
        <br></br>

        <Container id="main" fluid="fluid">
            <>
            <Card className="cardInfo mb-2">
                <Card.Body className="pt-2">
                    <>
                    <Row>
                        <div className="box_download">
                            <Col>
                            <img className="iconDownload" width={"60px"} src={ticket}></img>
                            </Col>
                            <br></br>
                            <br></br>
                            <br></br>
                        <div className={'titleDownload'} >
                            <h6 className="a"><b>Descarga exitosa</b></h6>
                            <h6 className="p">Tu póliza ha sido descargada con éxito.</h6>
                            <br></br>
                        </div>
                        </div>
                    </Row>
                    </>
                </Card.Body>
            </Card>
            </>
            <div className="btn-derecha-volver">
                <Button className="btn" variant="outline-info" onClick={backHome}>Volver</Button>
            </div>
        </Container>
        </>
    );
};

export default BoxPolicyDownload;
