import React from "react";
import ReactDOM from "react-dom";
import { useSelector, useDispatch } from "react-redux";
import { setPage, setKeytab } from "../../../actions";
/* Estilos */
import { Row, Col, Button, Card, Container } from "react-bootstrap";
import "../../../assets/sass/components/generales/box_client_message.scss";
import "../../../assets/sass/components/generales/cardInfo.scss";
import requerimiento from "../../../assets/img/requerimiento.png";
/* Import IMG */
import consorcio from "../../../assets/img/segurosAuto/consorcio.png";
import zurich from "../../../assets/img/segurosAuto/zurich.png";
import hdi from "../../../assets/img/segurosAuto/hdi.png";
import liberty from "../../../assets/img/segurosAuto/liberty.png";
import mapfre from "../../../assets/img/segurosAuto/mapfre.png";
import sura from "../../../assets/img/segurosAuto/sura.png";
import zenit from "../../../assets/img/segurosAuto/zenit.png";
import cardif from "../../../assets/img/segurosAuto/cardif.jpg";
import metlife from "../../../assets/img/segurosAuto/metlife.jpg";
import fourlife from "../../../assets/img/segurosAuto/4life.jpg";
import savebcj from "../../../assets/img/segurosAuto/savebcj.png";
import clc from "../../../assets/img/segurosAuto/clc.png";

const BoxClientMessage = (props) => {
    
    const dispatch = useDispatch();
    const backHome = () => {
        dispatch(setPage("PRELOADER"));
        setTimeout(() => {
            dispatch(setPage("HOME"));
        }, 2000);
    };
    const content = useSelector((state) => state.content)
    if (content.tipo_seguro === 'AsociadosCreditos'){
        dispatch(setKeytab('asociados_credito'));
    }else if (content.tipo_seguro === 'Creditos'){
        dispatch(setKeytab('otros'));  
    }else{
        dispatch(setKeytab(content.tipo_seguro.toLowerCase()));
    }
    const listImgCompany = {
        "consolidada" : zurich,
        "zurich" : zurich,
        "zenit" : zenit,
        "mapfre" : mapfre,
        "hdi" : hdi,
        "sura" : sura,
        "liberty" : liberty,
        "consorcio" : consorcio,
        "cardif" : cardif,
        "metlife" : metlife,
        "4life" : fourlife,
        "itau": savebcj,
        'savebcj': savebcj,
        "clc" : clc,
    }
    const companyImg = listImgCompany[content.dataBody.cf_compaia.toLowerCase()] ? listImgCompany[content.dataBody.cf_compaia.toLowerCase()] : content.company.toLowerCase();
    return (
        <>
        <br></br>
        <Container id="main" fluid="fluid">
            <>
            <Card className="cardInfo mb-2">                
                <Card.Body className="pt-2">                  
                    <Card.Title className="text-left" >
                        <Row>
                            <Col xs={8}>
                                
                            </Col>
                            <Col xs={4}>
                                <img width={"90px"} src={companyImg} alt={companyImg} className="card-image" ></img>
                            </Col>
                        </Row>
                    </Card.Title>
            
                    <div className="box_client_message">
                        <Col>
                            <img className="icon" width={"60px"} src={requerimiento}></img>
                        </Col>
                        
                        <br></br>
                        {content.idticket ?
                            <div className={'title_error'} >
                                <h6><b>El requerimiento n° {content.idticket ? content.idticket : ''} ha sido ingresado satisfactoriamente. </b></h6>
                                <h6>Se ha enviado un correo a {content.dataBody.email ? content.dataBody.email : ''} con información de la solicitud.</h6>
                                <br></br>
                                <div className="p" >
                                    <ul className='list-orange' >
                                        <li>Si contrataste tu seguro hace 5 o menos días hábiles, tu póliza se encuentra en proceso de emisión y llegará a tu correo  electrónico.</li>
                                    </ul>
                                </div>
                            </div>
                            :
                            <div className={'title_error'} >
                                <h6><b>Lo sentimos, no se pudo generar tu requerimiento. </b></h6>
                            </div>
                        }
                    </div>
                </Card.Body>
            </Card>
            </>

            <div className="btn-derecha-volver">
                <Button className="btn" variant="outline-info" onClick={backHome}>
                    Volver
                </Button>
            </div>
        </Container>
        </>
    );
};

export default BoxClientMessage;
