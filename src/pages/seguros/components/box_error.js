import React from 'react';
import ReactDOM from 'react-dom';
import { Container, Row, Col, Button, Alert, Breadcrumb, Card, Form, Accordion, ListGroup, Badge } from 'react-bootstrap';
import "../../../assets/sass/components/generales/box_error.scss";


const BoxError = (props) => {
    const url_insurances = process.env.REACT_APP_URL_VITRINA
    return (
        <div className="box_error">
            <Row>
                <Col xs={12} className="text-center">
                    <i className={'icon icon-itaufonts_exclamacao'}></i>

                    <div className={'title_error '} ><h6 className={'title_error '} >Lo sentimos, tuvimos un problema</h6></div>

                    <div className="content_error centrar_error" ><p>Estamos trabajando para solucionarlo.<br></br> Por favor, reintenta en unos minutos</p>
                    </div>

                    <div className="box_error_button">
                        <Button className="btn" variant="outline-info" href={url_insurances}>
                            Volver a Seguros
                        </Button>
                    </div>


                </Col>
            </Row>
        </div>
    )
}

export default BoxError;