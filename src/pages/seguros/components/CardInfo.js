import React, { useState } from "react";
import { Badge, Button, Card, Col, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { setPage, setContent, setShow, setKeytab } from "../../../actions";
import { toCapitalLetter } from "../../../utils/toCapitalLetter";
import PoliceService from "../../../services/PoliceService";
import cleanInsuranceName from "../../../utils/cleanInsuranceName";
import ModalInfo from "../components/modals/modal_info";
/* Estilos */
import "@sass-components/generales/cardInfo.scss";
import CoberturaService from "../../../services/CoberturaService";
/* Icono */
import comprobante from "../../../assets/img/icon_comprobante.png";
import { hideButtonSiniestro } from "../../../utils/hideButtonSiniestro";
import { polizasSoapZenit } from "../../../utils/polizasSoapZenit";

const CardInfo = ({
  Title,
  Subtitle,
  Parag,
  Parag2,
  Parag3,
  Parag4,
  Parag5,
  Parag6,
  icon = null,
  item,
  img,
  dateC,
  dateV,
  dateR,
  dateB,
  nAccount,
  tAccount,
  mAccount,
  url_siniestros,
  url_eliminar,
  codProduct,
  prima_bruta_mensual,
  prima_neta_mensual,
  rut,
  fecha_fin_poliza,
  nombre_completo,
  comision = null,
  estado_seguro,
  rut_tercero,
  fecha_contratacion,
  keytab,
  tipo_seguro
}) => {

  const { format } = require('rut.js')
  const dispatch = useDispatch();
  const page = useSelector((state) => state.page);
  const { getPolice } = PoliceService();
  const { getCobertura } = CoberturaService();
  const [disableButton, setdisableButton] = useState(false)
  const vidaButtons = ['VCA112', 'VCA1I2', 'VCA1T2', 'VCA212', 'VCA2I2', 'VCA2T2', 'VCA312', 'VCA3I2', 'VCA3T2', 'VCA4I2', 'VCA4T2']
  
  const urlPolizaZenit = "https://itausoap.zenitseguros.cl/ConsultaPoliza?Convenios=3E69B42AD35C51A12BEDCE07D70C6723&c=N"

  /* cargar pagina de detalles del seguro*/
  const detailsPage = (idInsurance) => {
    dispatch(setKeytab(idInsurance))
    dispatch(setContent(item));
    dispatch(setPage("PRELOADER"));
    setTimeout(() => {
      dispatch(setPage("DETAILS_INSURANCE"));
    }, 1000);
  };

  /* cargar pagina de voucher cuando no tiene comprobante*/
  const voucherPage = () => {
    dispatch(setContent(item));
    dispatch(setPage("PRELOADER"));
    setTimeout(() => {
      dispatch(setPage("VOUCHER"));
    }, 1000);
  };

  /* cargar poliza del seguro*/
  const loadPolice = () => {
    dispatch(setContent(item));
      const dataPolice = {
        nombre_cliente: item.nombre_completo ? item.nombre_completo : '',
        cod_producto: item.cod_producto ? item.cod_producto : '',
        rut: item.rut ? item.rut : '',
        numpol: item.num_poliza ? item.num_poliza : '',
        nomseg: item.segNombre ? cleanInsuranceName(item.segNombre) : '',
        numcredito: item.numcredito ?? '',
        company: item.compania_corto ? item.compania_corto.toLowerCase() : '',
        certificado: item.certificado ?? '',
        int_idventa: item.int_idventa  ?? '',
        type: 'copia',
        segNombre: item.segNombre ? item.segNombre : '',
        tipo_seguro: item.tipo_seguro ? item.tipo_seguro : '',
        managementByMok:  item.managementByMok ? item.managementByMok : false
      }
      if(item.num_poliza){
        getPolice(dataPolice);
      }else{
        dispatch(setPage("PRELOADER"));
        
        setTimeout(() => {
            dispatch(setPage("ERROR_NOINSURANCE"));
        }, 500);    
      }
   
  }

  /* Modal coberturas y asistencias*/
  const modalInfo = (codProduct, typebutton) => {
    getCobertura(codProduct, typebutton);
    setTimeout(() => {
      dispatch(setShow(true))
    }, 500);
  }

  /* Spinner/loader de la pagina de asistencias/denuncia*/
  const buttonLoaderAndDisable = (url, type) => {
    dispatch(setPage("PRELOADER"));
    setdisableButton(true)
    window.location.replace(url)
  }

  const atencionDelAsegurado = () => {
    dispatch(setPage("PRELOADER"));
    setTimeout(() => {
      (window.location.replace(url_eliminar));
    }, 1000);
    
  } 

  const downloadPDF = () => {
    // using Java Script method to get PDF file
    setTimeout(() => {
      fetch(item.pdf)
        .then(response => response.blob())
        .then(blob => {
          // Creating new object of PDF file
          const fileURL = window.URL.createObjectURL(blob);
          // Setting various property values
          let alink = document.createElement('a');
          let name = item.pdf.split('/');
          alink.href = fileURL;
          alink.download = name[4];
          alink.click();
        })
    }, 500);
  }
  
  return (
    <>
      <Card className="cardInfo mb-2">
        <Card.Body className="pt-2">
          <Card.Title className="text-left">
            {page === "HOME" ? (
              codProduct == 'SVBBSS026' || codProduct == 'SVBBSS040' ?
              <span className={"icon-x3 " + "icon-itaufonts_seguro_viagens"}> </span> :
              <span className={"icon-x3 " + icon}> </span>
            ) : null}

            {page === "DETAILS_INSURANCE" ? (
              <Row>
                <Col xs={8}>
                  <h5>Compañia : {Title}</h5>
                </Col>
                <Col xs={4}>
                  <img width={"90px"} src={img} alt={img} className="card-image" ></img>
                </Col>
              </Row>
            ) : null}



            {page === "VOUCHER" ? (
              <Row>
                <Col xs={8}>
                  <h5>Comprobante:</h5>
                </Col>
                <Col xs={4}>
                  <img width={"90px"} src={img} className="card-image" ></img>
                </Col>
              </Row>
            ) : null}
          </Card.Title>

          <Card.Text as="div" className="mb-2 mx-3">
            {page === "HOME" ? (
              <Row>
                <Col className="card-content-bordered" md={6}>
                  <div className="card-h4 mb-0">{Title ? Title.toLowerCase() : `Nombre seguro`}</div>
                  <Row>
                    <Col xs={10}>
                      <br></br>
                      {
                        item.gestion_requerimiento &&
                        <span className="text-capitalize">
                          <span>(Eliminación en progreso)</span>
                          <br></br>
                        </span>
                      }
                      {
                        Parag &&
                        <span className="text-capitalize">
                          <span className="card-text-bold">N° de póliza: </span>{Parag}
                          <br></br>
                        </span>
                      }
                      {
                        Parag2 ?
                          estado_seguro === 'contratado_r' && tipo_seguro==="Auto"  ?
                          <span className=" card-text-bold">
                            <span>(En proceso de evaluación)</span>
                            <br></br>
                            <span className="card-text-bold">Fecha de propuesta: </span>{fecha_contratacion}
                            <br></br>
                          </span>
                          :
                          <span className="text-capitalize">
                            <span className="card-text-bold">Vigencia: </span>{Parag2}
                            <br></br>
                          </span>
                          :
                          ''
                      }

                      
                      {
                        Parag3 &&
                        <span className="text-capitalize">
                          <span className="card-text-bold">Materia Asegurada: </span>{Parag3}
                          <br></br>
                        </span>
                      }
                      {
                        Parag5 &&
                        <span className="text-capitalize">
                          <span className="card-text-bold">Patente: </span>{Parag5}
                          <br></br>
                        </span>
                      }

                      {
                        estado_seguro === "contratado_r" && tipo_seguro==="Auto"  ?
                        <span className="text-capitalize">
                          <span className="card-text-bold">Rut Asegurado: </span>{rut_tercero}
                          <br></br>
                        </span>
                        :
                        ""
                      }
                      {
                        estado_seguro == "Pendiente" ? (
                          <span>
                            <span className="card-text-bold">Estado: </span>Pendiente de finalizar la
                            contratación.
                            <br></br>
                          </span>
                        ) : (
                          ""
                        )
                      }


                      {
                        Parag4 &&
                        <span className="text-capitalize">
                          <span className="card-text-bold">{ typeof item.tipo_pago == 'undefined' ? 'Prima bruta mensual: UF' : item.tipo_pago == 'Mensual' ? 'Prima bruta Mensual: UF' : item.tipo_pago== 'Unica' ? 'Prima bruta Única: UF' : 'Prima bruta Anual: UF'} </span>{Parag4}
                          <br></br>
                        </span>
                      }

                        {
                          estado_seguro =="Pendiente" ? <span>
                          <span className="card-text-bold">Estado: </span>Pendiente de finalizar la contratación.
                          <br></br>
                        </span> : ""
                        }
                      {
                        Parag6 &&
                        <span className="text-capitalize">
                          <span className="card-text-bold">Cantidad de trabajadores: </span>{Parag6}
                          <br></br>
                        </span>
                      }
                    </Col>
                    <Col xs={2} className="text-right pl-0 pr-4">
                      <div
                        onClick={() => detailsPage(keytab)}
                        className={
                          "icon-x3 icon-itaufonts_busca_consulta card-change-page"
                        }
                      ></div>
                      <label className="lbl_detail">Detalle</label>
                    </Col>
                  </Row>
                </Col>

                <Col md={3} className="text-center">
                  {
                    item.type != 'carga' &&
                      typeof item.pdf == 'undefined' ?
                      ''
                      :
                      item.pdf ?
                        <>
                          <div className={"icon-x3 icon-itaufonts_full_pdf"}></div>
                          {page === "HOME" ? (
                          codProduct == 'SVBBSS041' || codProduct == 'SVBBSS042' ?
                            <Button className="btn" variant="outline-info" onClick={() => downloadPDF()}>
                              Certificado
                            </Button>
                            :
                            <a href={item.pdf} download target="_blank">
                              <Button className="btn" variant="outline-info">
                                Certificado
                              </Button>
                            </a>
                          ) : null}
                        </>
                        :
                        <>
                          <img className="mb-1" src={comprobante}></img>
                          <Button className="btn" variant="outline-info"
                            onClick={() => voucherPage()}>
                            Comprobante
                          </Button>

                        </>

                  }
                </Col>

                <Col md={3} className="text-center">
                  <div className={"icon-x3 icon-itaufonts_full_pdf"}></div>
                  {
                    polizasSoapZenit.includes(codProduct.toUpperCase()) ?
                    <a href={urlPolizaZenit} target="_blank" >
                          <Button className="btn" variant="outline-info">
                            Póliza
                          </Button>
                        </a>
                        :
                    typeof item.url_poliza != 'undefined' ?
                      item.url_poliza ?
                        <a href={item.url_poliza} target="_blank">
                          <Button className="btn" variant="outline-info">
                            Póliza
                          </Button>
                        </a>
                        :
                        <Button onClick={() => loadPolice()} className="btn" variant="outline-info">
                          Póliza
                        </Button>
                      :
                      <Button onClick={() => loadPolice()} className="btn" variant="outline-info">
                        Póliza
                      </Button>
                  }
                </Col>
              </Row>
            ) : null}
            {page === "DETAILS_INSURANCE" ? (
              <>
                <Row>
                  <Col className="card-content-bordered-top card-h3 mb-2" xs={12}>
                    <Row>
                      <Col xs={4}>
                        <span className="card-text-bold">Fecha de Contratación: </span>
                      </Col>
                      <Col xs={8}>
                        {dateC}
                      </Col>
                    </Row>
                    {dateV ?
                      <Row>
                        <Col xs={4}>
                          <span className="card-text-bold">Fecha de Vigencia: </span>
                        </Col>
                        <Col xs={8}>
                          {dateV}
                        </Col>
                      </Row>
                      :
                      ""
                    }
                    {dateR ?
                      <Row>
                        <Col xs={4}>
                          <span className="card-text-bold">Fecha Renovación: </span>
                        </Col>
                        <Col xs={8}>
                          {dateR}
                        </Col>
                      </Row>
                      : ""}
                    {estado_seguro =="Pendiente" ?
                      <Row>
                        <Col xs={4}>
                          <span className="card-text-bold">Estado: </span>
                        </Col>
                        <Col xs={8}>
                          Pendiente de finalizar la contratación
                        </Col>
                      </Row>
                      : ""}
                    <Row>
                      <Col xs={4}>
                        {dateB == false ? <span className="card-text-bold"></span> : dateB.value ?  <span className="card-text-bold">Bonificación: </span>: <span className="card-text-bold"></span>}

                      </Col>
                      <Col xs={8}>
                        {dateB == false ? '' : dateB.value ? `La bonificación se realizará en un plazo de 30 días contados desde el pago de tu prima ${dateB.value}, esta bonificación se hará por parte de la compañía directamente a tu cuenta Itaú o por medio de un vale vista de no contar con dicha cuenta.`: ''}
                      </Col>
                    </Row>
                  </Col>
                </Row>
                {/*  <Row>
                  <Col className="card-content-bordered-top card-h3 mb-2" xs={12}>
                    <div className="card-text-bold">Medio de pago: </div>
                    {
                      nAccount ?
                        tAccount.toLowerCase() === "cc" ?
                          (<>
                            <Row><Col xs={12}><span>Cuenta corriente terminada en {nAccount}</span></Col> </Row>

                          </>
                          )

                          :
                          <>
                            <Row><Col xs={6}> <span>Tarjeta de crédito {mAccount} terminada en {nAccount}</span></Col></Row>
                          </>
                        :
                        'No tiene medios de pagos'
                    }
                  </Col>
                </Row> */}
                <Row className="justify-content-md-center">
                  <Button className="btn-white btn btn-primary" onClick={() => modalInfo(codProduct, 'coberturas')}>Mis coberturas</Button>
                  <Button className="btn-white btn btn-primary" onClick={() => modalInfo(codProduct, 'asistencia')}>Mis asistencias</Button>
                  {
                    hideButtonSiniestro(codProduct) ?
                      ''
                      :
                      <>
                        <Button className="btn-white btn btn-primary pl-3" disabled={disableButton} onClick={() => buttonLoaderAndDisable(url_eliminar + "&type_requirement=denuncio_de_siniestro")}>Denunciar siniestro</Button>
                        <Button className="btn-white btn btn-primary pl-3 btn_siniestro" href={url_siniestros}>Consultar siniestro</Button>
                      </>
                  }

                  {
                    vidaButtons.includes(codProduct.toUpperCase()) ?
                      <>
                        <Button className="btn-white btn btn-primary pl-3" disabled={disableButton} onClick={() => buttonLoaderAndDisable(url_eliminar)}>Realizar rescate</Button>
                        <Button className="btn-white btn btn-primary pl-3" disabled={disableButton} onClick={() => buttonLoaderAndDisable(url_eliminar)}>Consultar saldo</Button>
                      </>
                      :
                      ''
                  }
                </Row>


              </>
            ) : null}

            {page === "VOUCHER" ? (
              <>
                <Row>
                  <Col className="card-content-bordered-top card-h3 mb-2" xs={12}>

                    {/*------------Condicional---------*/}
                    <Row>
                      <Col xs={4}>
                        <span className="card-text-bold">Número de póliza: </span>
                      </Col>
                      <Col xs={8}>
                        {Parag ? Parag : 'Pendiente'}
                      </Col>
                    </Row>

                    {/*------------Condicional---------*/}
                    {Parag2 ?
                      <Row>
                        <Col xs={4}>
                          <span className="card-text-bold">Vigencia de póliza: </span>
                        </Col>
                        <Col xs={8}>
                          {Parag2}
                        </Col>
                      </Row>
                      :
                      ""
                    }

                    {Parag ?
                      <Row>
                        <Col xs={4}>
                          <span className="card-text-bold">Fin de póliza: </span>
                        </Col>
                        <Col xs={8}>
                          {fecha_fin_poliza}
                        </Col>
                      </Row>
                      :
                      ""
                    }


                    <Row>
                      <Col xs={4}>
                        <span className="card-text-bold">Compañía aseguradora: </span>
                      </Col>
                      <Col xs={8}>
                        {Title}
                      </Col>
                    </Row>

                    {prima_neta_mensual ?
                      <Row>
                        <Col xs={4}>
                          <span className="card-text-bold">Prima Neta Mensual UF: </span>
                        </Col>
                        <Col xs={8}>
                          {prima_neta_mensual}
                        </Col>
                      </Row>
                      :
                      ""
                    }

                    {prima_bruta_mensual ?
                      <Row>
                        <Col xs={4}>
                          <span className="card-text-bold">Prima Bruta Mensual UF: </span>
                        </Col>
                        <Col xs={8}>
                          {prima_bruta_mensual}
                        </Col>
                      </Row>
                      :
                      ""
                    }


                    <Row>
                      <Col xs={4}>
                        <span className="card-text-bold">RUT Contratante: </span>
                      </Col>
                      <Col xs={8}>
                        {format(rut)}
                      </Col>
                    </Row>

                    <Row>
                      <Col xs={4}>
                        <span className="card-text-bold">Nombre Contratante: </span>
                      </Col>
                      <Col xs={8}>
                        {nombre_completo}
                      </Col>
                    </Row>
                    <Row>
                      <Col xs={4}>
                        <span className="card-text-bold">Corredor de seguros: </span>
                      </Col>
                      <Col xs={8}>
                        ITAÚ CORREDORES DE SEGUROS LTDA.
                      </Col>
                    </Row>

                    {
                      comision ?
                        <Row>
                          <Col xs={4}>
                            <span className="card-text-bold">Comisión intermediación corredor: </span>
                          </Col>
                          <Col xs={8}>
                            {Math.round(comision)}%
                          </Col>
                        </Row>
                        :
                        ''
                    }
                    <br></br>
                    <Row>
                      <Col>
                        <span className="card-text-bold text-dark" >*La información de tus seguros contratados puede tener un desfase de hasta 60 días</span>
                      </Col>
                    </Row>
                    <br></br>
                    <Row>
                      <Col>
                        <span className="card-text-bold">La información presentada es solo un resumen del seguro contratado y no representa certificado de cobertura ni copia de pólizas</span>
                      </Col>
                    </Row>
                    <br></br>

                    <span id="printPageButton">
                    <Row>
                      <Col>
                        Si desea:
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <ul className='list-orange-v'>
                          <li> Copia de póliza</li>
                          <li> Denunciar siniestro</li>
                          <li> Devolución de primas</li>
                          <li> Eliminación de Seguros</li>
                          <li> Otros</li>
                        </ul>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        Solicítela <a className="atencion" onClick={atencionDelAsegurado}>aquí</a>
                      </Col>
                    </Row>
                    </span>

                  </Col>
                </Row>
              </>
            ) : null}
          </Card.Text>
        </Card.Body>
      </Card>
      <ModalInfo />
    </>
  );
};

export default CardInfo;
