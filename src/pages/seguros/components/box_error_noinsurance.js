import React from "react";
import {useState} from 'react';
import ReactDOM from "react-dom";
import { useSelector, useDispatch } from "react-redux";
import { setPage, setContent, setKeytab } from "../../../actions";
import PolicyRequirementService from "../../../services/PolicyRequirementService";

/* Estilos */
import { Row, Col, Button, Card, Container } from "react-bootstrap";
import "../../../assets/sass/components/generales/box_error_noinsurance.scss";
import "../../../assets/sass/components/generales/cardInfo.scss";

/* Import IMG */
import consorcio from "../../../assets/img/segurosAuto/consorcio.png";
import zurich from "../../../assets/img/segurosAuto/zurich.png";
import hdi from "../../../assets/img/segurosAuto/hdi.png";
import liberty from "../../../assets/img/segurosAuto/liberty.png";
import mapfre from "../../../assets/img/segurosAuto/mapfre.png";
import sura from "../../../assets/img/segurosAuto/sura.png";
import zenit from "../../../assets/img/segurosAuto/zenit.png";
import cardif from "../../../assets/img/segurosAuto/cardif.jpg";
import metlife from "../../../assets/img/segurosAuto/metlife.jpg";
import fourlife from "../../../assets/img/segurosAuto/4life.jpg";
import savebcj from "../../../assets/img/segurosAuto/savebcj.png";
import clc from "../../../assets/img/segurosAuto/clc.png";


const BoxErrorNoInsurance = (props) => {
    const {getPolicyRequirement} = PolicyRequirementService();
    const dispatch = useDispatch();
    const backHome = () => {
        dispatch(setPage("PRELOADER"));
        setTimeout(() => {
            dispatch(setPage("HOME"));
        }, 2000);
    };

    // VALIDACIÓN MAIL
    const [message, setMessage] = useState('');
    const [error, setError] = useState(null);

    //validar botón solicitar copia
    const [inputValue, setInputValue] = useState("");

    const content = useSelector((state) => state.content)

    if (content.tipo_seguro === 'AsociadosCreditos'){
        dispatch(setKeytab('asociados_credito'));
    }else if (content.tipo_seguro === 'Creditos'){
            dispatch(setKeytab('otros'));
    }else{
        dispatch(setKeytab(content.tipo_seguro.toLowerCase()));
    }

       const policyRequirement = () => {
        let isValid = /\S+@\S+\.\S+/.test(inputValue);

        if(!isValid){
            setError('Correo inválido');
        } else {
            setError('');
            //obtener fecha dd/mm/yy
            let today = new Date();
            let date=today.getDate() + "/"+ parseInt(today.getMonth()+1) +"/"+today.getFullYear();
            let segName     = content.segNombre + " " + content.fecha_contratacion;
        
            const dataPolice = {
                codigoseg: content.cod_producto ? content.cod_producto: '',
                numcredito: content.numcredito ? content.numcredito : '',
                tipocredito: content.tipocredito ? content.tipocredito : '',
                description: "Ingreso Web",
                subject: "Consulta del Asegurado",
                email: document.getElementById("email").value,
                name: content.nombre_completo ? content.nombre_completo:'',
                priority: 1,
                status: 2,
                cf_asunto_de_consulta: "Copia de p\u00f3liza o certificado",
                cf_motivo_consulta: "Copia P\u00f3liza",
                cf_poliza_consulta: segName ? segName:'',
                cf_rut_cliente: content.rut ? content.rut:'',
                cf_telefono_contacto:           "11111111",
                cf_submotivo_consulta:          "",
                cf_compaia: content.compania_corto ? content.compania_corto:'',
                cf_monto_devolucin:             "",
                cf_monto_devolucion_presente:   "NO",
                cf_nmero_de_pliza:content.num_poliza ? content.num_poliza:'',
                cf_origen: "Sitio Privado",
                cf_fecha_creacin: date,
                cf_es_fraude:                   "No",
            }

            getPolicyRequirement(dataPolice)
        }    
    }

    //IMÁGENES
    const listImgCompany = {
        "consolidada" : zurich,
        "zurich" : zurich,
        "zenit" : zenit,
        "mapfre" : mapfre,
        "hdi" : hdi,
        "sura" : sura,
        "liberty" : liberty,
        "consorcio" : consorcio,
        "cardif" : cardif,
        "metlife" : metlife,
        "4life" : fourlife,
        "itau" : savebcj,
        "savebcj" : savebcj,
        "clc" : clc,
    }

    const companyImg = listImgCompany[content.compania_corto.toLowerCase()] ? listImgCompany[content.compania_corto.toLowerCase()] : content.company.toLowerCase();

    return (
        <>
        <br></br>
        <Container id="main" fluid="fluid">
            <>
            <Card className="cardInfo mb-2">
                { content.num_poliza ? 
                <Card.Body className="pt-2">
                    <Card.Title className="text-left">
                        <Row>
                            <Col className="imgCompany">
                                <img width={"90px"} src={companyImg} alt={companyImg} className="card-image" ></img>
                            </Col>
                        </Row>
                    </Card.Title>
                    
                    <div className={'title_error '} >
                        <h6 className={'title_error_insurance '} ><b>Estimado (a) Cliente:</b><br></br>
                        <br></br>Tu póliza no se encuentra disponible en este momento, para solicitar una copia, ingresa tu correo electrónico:</h6>
                    </div>
                 
                  
                    <form onClick={(e) => e.preventDefault()}>
                        <div className= {'email'} >
                            <label >Correo: </label>
                            <input className={'input'} type="email" required id="email" name="email" value={inputValue}{...message} onChange={ (e) => setInputValue(e.target.value, setError(''))}>
                            </input>
                            {error && <h6 className={'error'} style={{color: 'red'}}>{error}</h6>}
                        </div>
                        <div className="btn-center">
                            <Button onClick={() => policyRequirement()} disabled={inputValue === ""} type="submit" className="btn-white btn btn-primary" >Solicitar copia</Button>
                        </div>
                    </form>
                  
                </Card.Body>
                :
                <div className="box_error_noinsurance text-center">
                    <Row>
                        <Col className="text-center">
                            <div className={'title_error'} ><h6 className={'title_error_insurance2 '} ><br></br>Esta copia de póliza no se encuentra disponible por el momento.</h6></div>
                            
                            <div className="content_error" >
                                <ul className='list-orange'>
                                    <li>Si contrataste tu seguro hace 5 o menos días hábiles, tu póliza se encuentra en proceso de emisión y llegará a tu correo  electrónico.</li>
                                    <li>Puedes solicitarla a través de nuestro contact center al 600 600 1200.</li>
                                </ul>
                            </div>
                        </Col>
                    </Row>
                </div>
                }
            </Card>
            </>

            <div className="btn-derecha-volver">
                <Button className="btn" variant="outline-info" onClick={backHome}>
                    Volver
                </Button>
            </div>
        </Container>
        </>
  );
};

export default BoxErrorNoInsurance;
