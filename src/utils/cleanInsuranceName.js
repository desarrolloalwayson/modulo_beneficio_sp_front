const cleanInsuranceName = (insuranceName) => {
    const insuranceLowerCase = insuranceName.toLowerCase();
    const mapObj = {
        "seg ": "seguro ",
        "cl?sico": "clásico",
        "dig d": "deducible"
    };
    const cleanName = insuranceLowerCase.replace(/\b(?:seg\s|cl\?sico|dig\sd)\b/gi, matched => mapObj[matched]);
    return cleanName;
}

export default cleanInsuranceName;