import { useSelector, useDispatch } from "react-redux";
import { setPage, setPreloader, setContent } from "../actions";
import { genericPost } from "./GenericPost";

function PoliceService() {

    const rut = localStorage.getItem('kPLUSyn') || process.env.REACT_APP_RUT
    const api = process.env.REACT_APP_API_URL
    const dispatch = useDispatch();
    const content = useSelector((state) => state.content)

    async function getPolice(policeData) {
        const saveBcjPol = {
            "DESHIPIND" : true,
            "DESHIPINDA" : true,
            "DESHIPIND1" : true
        }

        let togglePath = "defaultpath";

        if(policeData.managementByMok){
            togglePath = "mokpath";
        }
        if(saveBcjPol[policeData.cod_producto]){
            togglePath = "savebcjpath";
        }

        const PolicyPathList = {
            "savebcjpath" : `itau/saveBCJ/getCopyPolicy`,
            "mokpath" : `itau/wsMOK/getCopyPolicyMOK`,
            "defaultpath" :`itau/policy/getDocument`
        }

        const path =  PolicyPathList[togglePath];
        const provider = togglePath == "mokpath" ? "MOK" : policeData.company;

        const dataBody = {
            "nombre_cliente": policeData.nombre_cliente,
            "rut": rut,
            "numpol": policeData.numpol,
            "nomseg": policeData.nomseg,
            "creditnumber": policeData.numcredito,
            "company": policeData.company,
            "certificado": policeData.certificado,
            "codigoinbroker": policeData.cod_producto,
            "int_idventa" : policeData.int_idventa,
            "provider" : provider,
            "insurance_category": policeData.tipo_seguro,
            "module" : "Modulo de Seguros",
            "origin" : "Sitio Privado",
            "type": policeData.type
        };

        dispatch(setPage("PRELOADER"));
        genericPost(`${api}/${path}`, dataBody)
            .then((res) => {
                dispatch(setPreloader('INACTIVE'));
                if (res.data.error || !res.data.success || res.data.code == 503) {
                    dispatch(setPage("ERROR_NOINSURANCE"));
                } else if (res.data.content) {
                    //descargar base64
                    const linkSource = `data:application/pdf;base64,${res.data.content}`;
                    const downloadLink = document.createElement("a");
                    const fileName = `${policeData.rut.replace('-','')}_${policeData.numpol}.pdf`;

                    downloadLink.href = linkSource;
                    downloadLink.download = fileName;
                    downloadLink.click();

                    dispatch(setPage("POLICY_DOWNLOAD"));

                } else {
                    dispatch(setPage("ERROR_NOINSURANCE"));
                }
            })
            .catch((err) => {
                dispatch(setPreloader('INACTIVE'));
                dispatch(setPage("ERROR_NOINSURANCE"));
            });
    }

    return { getPolice }
}

export default PoliceService;