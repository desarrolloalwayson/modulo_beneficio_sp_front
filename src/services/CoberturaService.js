import { useDispatch } from "react-redux";
import { setCoberturas, setPage, setPreloader } from "../actions";
import { genericGet } from "./GenericGet";

function CoberturaService() {

    const rut = localStorage.getItem('kPLUSyn') || process.env.REACT_APP_RUT
    const base = process.env.REACT_APP_API_URL
    const dispatch = useDispatch();

    async function getCobertura(codProduct, typebutton) {
      
        genericGet(`${base}/itau/inssurance/coverage/${codProduct}`)
            .then((res) => {
                dispatch(setPreloader('INACTIVE'));
                if (res.data.error || res.data.code == 503) {
                    dispatch(setPage("ERROR_NOINSURANCE"));
                } else {
                    
                    if(Object.keys(res.data.content).length){  
                        const resultData = res.data.content;
                        dispatch(setCoberturas({'tipo' : typebutton, 'detalles' : typeof resultData[typebutton.toUpperCase()] != 'undefined' ?  resultData[typebutton.toUpperCase()] : [], "phone" : typeof resultData.FONO_ASIST != 'undefined' ? resultData.FONO_ASIST : "", "comp" : typeof resultData.EMPR_ASIST != 'undefined' ? resultData.EMPR_ASIST : ""} ));
                    }else{
                        dispatch(setCoberturas({'tipo': typebutton, 'detalles':`El seguro no posee ${typebutton == 'coberturas' ? 'coberturas' : 'asistencias'} asociadas`}));
                    }
                }
               
            })

            .catch((err) => {
                dispatch(setPreloader('INACTIVE'));
                console.log(err, 'err');
                    setTimeout(() => {
                        dispatch(setPage("ERROR"));
                    }, 3000);
                
            });
    }
    
    return { getCobertura }
}

export default CoberturaService;