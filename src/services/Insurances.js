import { useDispatch } from "react-redux";
import { setListInsurances, setPage, setPreloader } from "../actions";
import { genericGet } from "./GenericGet";

const Insurances = () => {

    const rut = localStorage.getItem('kPLUSyn') || process.env.REACT_APP_RUT
    const base = process.env.REACT_APP_API_URL
    const dispatch = useDispatch();

    async function getInsurances() {
        dispatch(setPage("PRELOADER"));
        genericGet(`${base}/itau/inssurance/display/${rut}`)
            .then((res) => {
                dispatch(setPreloader('INACTIVE'));
                if (res.data.error || res.data.code == 503) {
                    setTimeout(() => {
                        dispatch(setPage("ERROR"));
                    }, 2000);
                    
                } else {
                    dispatch(setListInsurances(res.data.content));
                    dispatch(setPage("HOME"));
                }
            })
            .catch((err) => {
                dispatch(setPreloader('INACTIVE'));
                console.log(err, 'err');
                    setTimeout(() => {
                        dispatch(setPage("ERROR"));
                    }, 3000);
                
            });
    }

    return { getInsurances };
};

export default Insurances;