import axios from 'axios';

export const genericPost = (url, body, headersHttp = {}) => {
    return new Promise((resolve, reject) => {
        try {
            axios
                .post( url, body, headersHttp)
                .then((res) => {
                    resolve(res);
                })
                .catch((err) => {
                    reject("Error in genericPost axios!");
                });
        } catch (error) {
            //console.log("estoy en el error 1", error);
            reject(error);
        }
    });
};