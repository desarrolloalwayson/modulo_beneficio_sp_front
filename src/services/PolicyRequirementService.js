import { useSelector, useDispatch } from "react-redux";
import { setPage, setPreloader, setContent } from "../actions";
import { genericPost } from "./GenericPost";

function PolicyRequirementService() {

    const rut = localStorage.getItem('kPLUSyn') || process.env.REACT_APP_RUT
    const api = process.env.REACT_APP_API_URL
    const dispatch = useDispatch();

    const content = useSelector((state) => state.content)

    async function getPolicyRequirement(policeData) {        
        const path =  'itau/copyPolicyRequirement/getCopyPolicyRequirement';

        const dataBody = {
            codigoseg:                      policeData.codigoseg,
            numcredito:                     policeData.numcredito,
            tipocredito:                    policeData.tipocredito,
            description:                    policeData.description,
            subject:                        policeData.subject,
            email:                          policeData.email,
            name:                           policeData.name,
            priority:                       policeData.priority,
            status:                         policeData.status,
            cf_asunto_de_consulta:          policeData.cf_asunto_de_consulta,
            cf_motivo_consulta:             policeData.cf_motivo_consulta,
            cf_poliza_consulta:             policeData.cf_poliza_consulta,
            cf_rut_cliente:                 policeData.cf_rut_cliente,
            cf_telefono_contacto:           policeData.cf_telefono_contacto,
            cf_submotivo_consulta:          policeData.cf_submotivo_consulta,
            cf_compaia:                     policeData.cf_compaia,
            cf_monto_devolucin:             policeData.cf_monto_devolucin,
            cf_monto_devolucion_presente:   policeData.cf_monto_devolucion_presente,
            cf_nmero_de_pliza:              policeData.cf_nmero_de_pliza,
            cf_origen:                      policeData.cf_origen,
            cf_fecha_creacin:               policeData.cf_fecha_creacin,   
            cf_es_fraude:                   policeData.cf_es_fraude
        };

        const data = {dataBody:dataBody, idticket:'', company: content.company, tipo_seguro: content.tipo_seguro};
        
        dispatch(setPage("PRELOADER"));
        genericPost(`${api}/${path}`, dataBody)
            .then((res) => {
                dispatch(setPreloader('INACTIVE'));
              
                if (res.data) {
                    data['idticket'] = res.data;
                
                    dispatch(setContent(data));
                    dispatch(setPage("CLIENT_MESSAGE"));

                } else {                
                    dispatch(setContent(data));
                    dispatch(setPage("CLIENT_MESSAGE"));
                }
            })
            .catch((err) => {
                dispatch(setPreloader('INACTIVE'));
                console.log(err, 'err');                
                dispatch(setContent(data));
                dispatch(setPage("CLIENT_MESSAGE"));
            });
    }

    return { getPolicyRequirement }
}

export default PolicyRequirementService;